<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Student Collection Form</title>

    <style>
        .headerSection{
            height: 100px;
            background-color: dimgray;
            border: solid white 1px;
        }

        .headerSection h2{
            margin-top: 35px;
            padding: 0;
            text-align: center;
            color: white;
        }

        .mainArea{
            min-height: 500px;
            background-color: whitesmoke;
            text-align: center;
        }

       .fromDiv{
           margin:auto;
          width: 300px;
           text-align: center;

       }
        .footerSection{
            height: 50px;
            background-color: dimgray;
            border: solid white 1px;
        }
        .footerSection p{
            margin-top: 20px;
            padding: 0;
            text-align: center;
            color: white;
        }

    </style>
</head>
<body>
    <div>
        <section class="headerSection">
            <h2>Student Information Collection Form</h2>

        </section>
        <hr>
        <section class="mainArea">
            <div class="fromDiv">

            <form action="process.php" method="post">


                    <table border="1">

                        <tr>
                            <td colspan="2">Student Information</td>

                        </tr>

                        <tr>
                            <td>Student Name:</td>
                            <td><input type="text" name="studentName"></td>


                        </tr>
                        <tr>
                            <td>Student Roll:</td>
                            <td><input type="number" name="studentRoll"></td>

                        </tr>

                    </table>

                    <br><br>

                    <table border="1">
                        <tr>
                            <td colspan="2">Course Information</td>

                        </tr>


                        <tr>
                            <td>Bangla:</td>
                            <td><input type="number" name="banglaMarks"></td>
                        </tr>
                        <tr>
                            <td>English:</td>
                            <td><input type="number" name="englishMarks"></td>


                        </tr>

                        <tr>
                        <td>Math:</td>
                        <td><input type="number" name="mathMarks"></td>
                        </tr>



                    </table>
                    <br>  <br>

                    <table border="1">

                        <tr>

                            <td><input type="submit" name="Submit"></td>


                        </tr>
                    </table>



            </form>
            </div>

        </section>

        <section class="footerSection">
            <p> www.abcschool.com</p>
        </section>

    </div>
</body>
</html>