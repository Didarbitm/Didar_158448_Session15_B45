<?php
/**
 * Created by PhpStorm.
 * User: Web App Develop-PHP
 * Date: 1/21/2017
 * Time: 3:04 PM
 */

namespace App;


class Course
{
    private $banglaMarks;
    private $englishMarks;
    private $mathMarks;

    private $banlaGrade;
    private $englishGrade;
    private $mathGrade;


    public function setBanglaMarks($banglaMarks)
    {
        $this->banglaMarks = $banglaMarks;
    }

    public function getBanglaMarks()
    {
        return $this->banglaMarks;
    }


    public function setEnglishMarks($englishMarks)
    {
        $this->englishMarks = $englishMarks;
    }

    public function getEnglishMarks()
    {
        return $this->englishMarks;
    }


    public function setMathMarks($mathMarks)
    {
        $this->mathMarks = $mathMarks;
    }

    public function getMathMarks()
    {
        return $this->mathMarks;
    }

    public function markToGrade(){


    }


    public function setBanlaGrade()
    {
        $this->banlaGrade = $this->marksTOGrade($this->banglaMarks);
    }

    public function getBanlaGrade()
    {
        return $this->banlaGrade;
    }


    public function setEnglishGrade()
    {
        $this->englishGrade = $this->marksTOGrade($this->englishMarks);
    }

    public function getEnglishGrade()
    {
        return $this->englishGrade;
    }




    public function setMathGrade()
    {
        $this->mathGrade = $this->marksTOGrade($this->mathMarks);;
    }

    public function getMathGrade()
    {
        return $this->mathGrade;
    }




    public function marksTOGrade($mark){

        switch ($mark){
            case ( $mark>79);
                return "A+";
                break;
            case ( $mark<80 && $mark>69);
                return "A";
                break;
            case ( $mark<70 && $mark>59);
                return "A-";
                break;
            case ( $mark<60 && $mark>49);
                return "B";
                break;
            case ( $mark<50 && $mark>39);
                return "C";
                break;
            case ( $mark<40 && $mark>33);
                return "D";
                break;
            case ( $mark<32 && $mark>=0);
                return "F";
                break;
        }
    }

}