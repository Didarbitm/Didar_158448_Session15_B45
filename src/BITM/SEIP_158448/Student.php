<?php


namespace App;


class Student
{
    private $studentName;
    private $studentRoll;


    public function setStudentName($studentName)
    {
        $this->studentName = $studentName;
    }

    public function setStudentRoll($studentRoll)
    {
        $this->studentRoll = $studentRoll;
    }



    public function getStudentName()
    {
        return $this->studentName;
    }


    public function getStudentRoll()
    {
        return $this->studentRoll;
    }



}